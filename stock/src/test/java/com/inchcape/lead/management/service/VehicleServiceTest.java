package com.inchcape.lead.management.service;

import com.inchcape.lead.management.entity.Vehicle;
import com.inchcape.lead.management.repository.VehicleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.*;

public class VehicleServiceTest {

    @Mock
    private VehicleRepository vehicleRepository;

    @InjectMocks
    private VehicleService vehicleService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetVehicleById() {
        // Given
        Long id = 1L;
        Vehicle vehicle = new Vehicle();
        when(vehicleRepository.findById(id)).thenReturn(Optional.of(vehicle));

        // When
        Vehicle result = vehicleService.getVehicleById(id);

        // Then
        assertThat(result).isNotNull();
        assertSame(vehicle, result);
    }

    @Test
    public void testCreateVehicle() {
        // Given
        Vehicle vehicle = new Vehicle();
        when(vehicleRepository.save(vehicle)).thenReturn(vehicle);

        // When
        Vehicle result = vehicleService.createVehicle(vehicle);

        // Then
        assertThat(result).isNotNull();
        assertSame(vehicle, result);
    }

    @Test
    public void testUpdateVehicle() {
        // Given
        Long id = 1L;
        Vehicle vehicle = new Vehicle();
        when(vehicleRepository.findById(id)).thenReturn(Optional.of(vehicle));
        when(vehicleRepository.save(vehicle)).thenReturn(vehicle);

        // When
        Vehicle result = vehicleService.updateVehicle(id, vehicle);

        // Then
        assertThat(result).isNotNull();
        assertSame(vehicle, result);
    }

    @Test
    public void testDeleteVehicle() {
        // Given
        Long id = 1L;

        // When
        vehicleService.deleteVehicle(id);

        // Then
        verify(vehicleRepository, times(1)).deleteById(id);
    }
}

