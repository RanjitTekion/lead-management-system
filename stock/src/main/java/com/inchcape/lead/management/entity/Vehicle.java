package com.inchcape.lead.management.entity;

import com.inchcape.lead.management.entity.enums.FuelType;
import com.inchcape.lead.management.entity.enums.Status;
import com.inchcape.lead.management.entity.enums.Transmission;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import jakarta.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="vehicle")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime created;

    private LocalDateTime modified;

    private Integer bhp;

    private BigDecimal co2Emissions;

    private BigDecimal combinedFuelConsumption;

    private Integer doors;

    private BigDecimal engineSize;

    private String exteriorColour;

    @Enumerated(EnumType.STRING)
    private FuelType fuelType;

    @Lob
    private byte[] images;

    @Column(length = 2000)
    private String keyFeatures;

    @ManyToOne
    private Make make;

    private String model;

    private BigDecimal price;

    private String regNumber;

    private LocalDate regDate;

    private Integer seats;

    @Column(length = 2000)
    private String specification;

    private String stockNumber;

    @ManyToOne
    private StockType stockType;

    @Enumerated(EnumType.STRING)
    private Transmission transmission;

    @ManyToOne
    private VehicleType vehicleType;

    @Enumerated(EnumType.STRING)
    private Status status;

}


