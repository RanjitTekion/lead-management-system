package com.inchcape.lead.management.repository;

import com.inchcape.lead.management.entity.Vehicle;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class VehicleRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Test
    public void testSaveVehicle() {
        // Given
        Vehicle vehicle = createVehicle();

        // When
        Vehicle savedVehicle = vehicleRepository.save(vehicle);

        // Then
        assertThat(savedVehicle.getId()).isNotNull();
        assertThat(savedVehicle).isEqualToIgnoringGivenFields(vehicle, "id");
    }

    @Test
    public void testFindAll() {
        // Given
        Vehicle vehicle1 = createVehicle();
        Vehicle vehicle2 = createVehicle();
        entityManager.persistAndFlush(vehicle1);
        entityManager.persistAndFlush(vehicle2);

        // When
        List<Vehicle> vehicles = vehicleRepository.findAll();

        // Then
        assertThat(vehicles).hasSize(2);
    }

    @Test
    public void testFindById() {
        // Given
        Vehicle vehicle = createVehicle();
        entityManager.persistAndFlush(vehicle);

        // When
        Vehicle found = vehicleRepository.findById(vehicle.getId()).orElse(null);

        // Then
        assertThat(found).isNotNull();
        assertThat(found).isEqualTo(vehicle);
    }

    @Test
    public void testDeleteById() {
        // Given
        Vehicle vehicle = createVehicle();
        entityManager.persistAndFlush(vehicle);

        // When
        vehicleRepository.deleteById(vehicle.getId());

        // Then
        assertThat(vehicleRepository.findById(vehicle.getId())).isEmpty();
    }

    private Vehicle createVehicle() {
        Vehicle vehicle = new Vehicle();
        vehicle.setModel("Camry");
        return vehicle;
    }
}

