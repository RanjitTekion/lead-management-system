package com.inchcape.lead.management.controller;

import com.inchcape.lead.management.entity.Vehicle;
import com.inchcape.lead.management.service.VehicleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@WebMvcTest(VehicleController.class)
public class VehicleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VehicleService mockVehicleService;

    @InjectMocks
    private VehicleController vehicleController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllVehicles() throws Exception {
        // Given
        Vehicle vehicle1 = new Vehicle();
        Vehicle vehicle2 = new Vehicle();
        List<Vehicle> vehicles = Arrays.asList(vehicle1, vehicle2);

        // Mocking the service method
        when(mockVehicleService.getAllVehicles()).thenReturn(vehicles);

        // Performing GET request to /vehicles endpoint
        mockMvc.perform(MockMvcRequestBuilders.get("/vehicles"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }

    @Test
    public void testGetVehicleById() throws Exception {
        // Given
        Long id = 1L;
        Vehicle vehicle = new Vehicle();
        when(mockVehicleService.getVehicleById(anyLong())).thenReturn(vehicle);

        // Performing GET request to /vehicles/{id} endpoint
        mockMvc.perform(MockMvcRequestBuilders.get("/vehicles/{id}", id))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(id));
    }

    @Test
    public void testCreateVehicle() throws Exception {
        // Given
        String requestBody = "{ \"make\": \"Toyota\", \"model\": \"Camry\", \"year\": 2022, \"color\": \"Silver\", \"transmission\": \"AUTOMATIC\", \"fuelType\": \"PETROL\", \"price\": 25000.00, \"status\": \"AVAILABLE\", \"doors\": 4, \"engineSize\": 2.5, \"seats\": 5, \"combinedFuelConsumption\": 30.5, \"images\": [\"image1.jpg\", \"image2.jpg\"], \"keyFeatures\": [\"Key Feature 1\", \"Key Feature 2\"], \"regNumber\": \"ABC123\", \"regDate\": \"2022-01-01\", \"specification\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\", \"stockNumber\": \"STK123\", \"makeId\": 1, \"vehicleTypeId\": 1, \"stockTypeId\": 1 }";

        // When
        mockMvc.perform(MockMvcRequestBuilders.post("/vehicles")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testUpdateVehicle() throws Exception {
        // Given
        Long id = 1L;
        String requestBody = "{ \"make\": \"Toyota\", \"model\": \"Camry\", \"year\": 2022, \"color\": \"Silver\", \"transmission\": \"AUTOMATIC\", \"fuelType\": \"PETROL\", \"price\": 25000.00, \"status\": \"AVAILABLE\", \"doors\": 4, \"engineSize\": 2.5, \"seats\": 5, \"combinedFuelConsumption\": 30.5, \"images\": [\"image1.jpg\", \"image2.jpg\"], \"keyFeatures\": [\"Key Feature 1\", \"Key Feature 2\"], \"regNumber\": \"ABC123\", \"regDate\": \"2022-01-01\", \"specification\": \"Lorem ipsum dolor sit amet, consectetur adipiscing elit.\", \"stockNumber\": \"STK123\", \"makeId\": 1, \"vehicleTypeId\": 1, \"stockTypeId\": 1 }";

        // When
        mockMvc.perform(MockMvcRequestBuilders.put("/vehicles/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testDeleteVehicle() throws Exception {
        // Given
        Long id = 1L;

        // Performing DELETE request to /vehicles/{id} endpoint
        mockMvc.perform(MockMvcRequestBuilders.delete("/vehicles/{id}", id))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // Verifying that the service method is called with the correct ID
        verify(mockVehicleService, times(1)).deleteVehicle(id);
    }
}


